/*
const { Schema, model } = require("mongoose");
const Db = require("../db/db");
const httpResp = require("../tools/httpResp");
const TokenManager = require("../tools/TokenManager");


const userSchema = new Schema({
    username: String,
    email: String,
    password: String
}, { collection: 'users' });

userSchema.statics.findByEmail = async function (user) {
    try {
        await Db.connect();
        const data = await this.findOne({ email: user.email });
        if (data && await comparePassword(user.password, data.password)) {
            data.password = null;
            // return httpResp(200, { user: data, token: TokenManager.generateToken({ email: data.email }) });
            return httpResp(200, { user: data });
        }
        return httpResp(400, null, error = "Email or password incorrect");
    } catch (error) {
        console.log("db requrest find by email failed", error);
    } finally {
        await Db.close();
    }
}

userSchema.statics.register = async function (user) {
    try {
        await Db.connect();
        user.password = await hashPassword(user.password)
        return this.create(user)
    } catch (error) {
        console.log("db requrest find by email failed", error);
    } finally {
        await Db.close();
    }
}

async function hashPassword(password) {
    const salt = await bcrypt.genSalt(10);
    return bcrypt.hash(password, salt);
}

async function comparePassword(plainPassword, hashPassword) {
    return bcrypt.compareSync(plainPassword, hashPassword)
}

const User = model('User', userSchema);
module.exports = User;
*/