const httpResp = (status = 200, data = null, error = null) => {
    return {
        status: status,
        data: data,
        error: error
    }
}

module.exports = httpResp