const jwt = require('jsonwebtoken');
const { token: { secret, duration } } = require('../config')

// const secret = process.env.DEV_TOKEN_SECRET;
// const duration = process.env.DEV_TOKEN_DURATION;

module.exports = class TokenManager {
    static generateToken(data) {
        return jwt.sign(data, secret, {
            expiresIn: 86400,
            algorithm: "HS256"
        })
    }

    static verifyToken(token, callback) {
        jwt.verify(token, secret, callback(err, decode))
    }
}