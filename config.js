const env = process.env.NODE_ENV;

const prod = {
    app: {
        port: parseInt(process.env.PROD_APP_PORT)
    },
    db: {
        host: process.env.PROD_DB_HOST,
        port: parseInt(process.env.PROD_DB_PORT),
        name: process.env.PROD_DB_NAME
    },
    token: {
        secret: process.env.PROD_TOKEN_SECRET,
        duration: process.env.PROD_TOKEN_DURATION
    }
}

const dev = {
    app: {
        port: parseInt(process.env.DEV_APP_PORT) || 5000,
        base_url: parseInt(process.env.DEV_APP_BASE_URL) || '/api/node'
    },
    db: {
        host: process.env.DEV_DB_HOST || 'localhost',
        port: parseInt(process.env.DEV_DB_PORT) || 27017,
        name: process.env.DEV_DB_NAME || 'crudTest'
    },
    token: {
        secret: process.env.DEV_TOKEN_SECRET,
        duration: parseInt(process.env.DEV_TOKEN_DURATION)
    }
}

const config = {
    prod,
    dev
}

module.exports = config[env];