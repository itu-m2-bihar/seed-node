const { default: mongoose } = require("mongoose");
const { db: { host, port, name } } = require('../config');


module.exports = class Db {
    static async connect() {
        try {
            return await mongoose.connect(`mongodb://${host}:${port}/${name}`);
        } catch (error) {
            console.log("db connection failed", error);
        }
    }

    static async close() {
        await mongoose.connection.close();
    }
}