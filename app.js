require('dotenv').config();
const { app: { port, base_url } } = require('./config');
const express = require('express');
const TokenManager = require('./tools/TokenManager');
const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

/**
 * Middleware evey request will pass here
 * Used for checking token,...
 */
app.use(`${base_url}/`, (req, res, next) => {
    next()
});

// app.get(`${base_url}/get`, (req, res) => {
//     let msg = 'GET ' + port;
//     res.status(200);
//     res.end(msg);
// })

app.use(`${base_url}/blog`, require('./controller/blogController'));

app.post(`${base_url}/genToken`, (req, res) => {
    res.json({ token: TokenManager.generateToken(req.body.user) });
})

const server = app.listen(port, () => {
    console.log('Listening at port:%s', port);
});
